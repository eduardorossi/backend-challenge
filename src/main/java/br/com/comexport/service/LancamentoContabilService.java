package br.com.comexport.service;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import br.com.comexport.domain.EstatisticaLancamentoContabil;
import br.com.comexport.domain.LancamentoContabil;
import br.com.comexport.storage.LancamentoContabilStorage;

@Service
public class LancamentoContabilService {

	public UUID criarLancamentoContabil(LancamentoContabil lancamentoContabil) throws NoSuchAlgorithmException {
		return LancamentoContabilStorage.adicionar(lancamentoContabil);
	}

	public br.com.comexport.domain.LancamentoContabil getLancamentoContabilPeloId(UUID id) {
		return LancamentoContabilStorage.getLancamentoContabilPeloId(id);
	}

	public List<LancamentoContabil> getLancamentoContabilPelaContaContabil(String contaContabil) {
		return LancamentoContabilStorage.getLancamentoContabilPelaContaContabil(contaContabil);
	}

	public EstatisticaLancamentoContabil getEstatisticasLancamentosContabeis() {
		return LancamentoContabilStorage.getEstatisticasLancamentosContabeis();
	}

	public EstatisticaLancamentoContabil getEstatisticasLancamentosContabeis(String contaContabil) {
		return LancamentoContabilStorage.getEstatisticasLancamentosContabeis(contaContabil);
	}

	public boolean verificarExistenciaMesmoLancamento(LancamentoContabil lancamentoContabil) {
		return LancamentoContabilStorage.verificarExistenciaMesmoLancamento(lancamentoContabil);
	}

}
