package br.com.comexport.storage;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import br.com.comexport.domain.EstatisticaLancamentoContabil;
import br.com.comexport.domain.LancamentoContabil;

public class LancamentoContabilStorage {

	private static List<LancamentoContabil> lancamentosContabeis = new ArrayList<>();
	
	public static UUID adicionar(LancamentoContabil lancamentoContabil) throws NoSuchAlgorithmException {
		
		UUID id = UUID.randomUUID();
		
		lancamentoContabil.setId(id);
		
		lancamentosContabeis.add(lancamentoContabil);
		
		return id; 
	}
	
	public static LancamentoContabil getLancamentoContabilPeloId(UUID id) {
		
		LancamentoContabil lancamentoContabil = null;
		
		if (!lancamentosContabeis.isEmpty()) {
		
			lancamentoContabil = lancamentosContabeis.stream()
					.filter(lancamento -> id.equals(lancamento.getId()))
					.collect(Collectors.reducing((a, b) -> null))
					.orElse(null);
		
		}
		
		return lancamentoContabil;
	}

	public static List<LancamentoContabil> getLancamentoContabilPelaContaContabil(String contaContabil) {
		
		List<LancamentoContabil> lancamentos = null;
		
		if (!lancamentosContabeis.isEmpty()) {
			
			lancamentos = lancamentosContabeis.stream()
				.filter(lancamento -> contaContabil.equals(lancamento.getContaContabil()))
				.collect(Collectors.toList());
		}
		
		return lancamentos;
	}

	public static EstatisticaLancamentoContabil getEstatisticasLancamentosContabeis() {
		
		EstatisticaLancamentoContabil estatistica = null;
		
		if (!lancamentosContabeis.isEmpty()) {
			
			estatistica = new EstatisticaLancamentoContabil();
		
			estatistica.setQtde(lancamentosContabeis.size());
			
			estatistica.setMax(
					lancamentosContabeis.stream()
					.max(Comparator.comparing(LancamentoContabil::getValor)).get().getValor());
			
			estatistica.setMin(
					lancamentosContabeis.stream()
					.min(Comparator.comparing(LancamentoContabil::getValor)).get().getValor());
			
			BigDecimal total = lancamentosContabeis.stream()
					.map(lancamento -> lancamento.getValor()).reduce(BigDecimal.ZERO, BigDecimal::add);
			
			estatistica.setSoma(total);
			
			estatistica.setMedia(total.divide(new BigDecimal(estatistica.getQtde())));
		}
		
		return estatistica;
	}

	public static EstatisticaLancamentoContabil getEstatisticasLancamentosContabeis(String contaContabil) {
		
		EstatisticaLancamentoContabil estatistica = null;
		
		if (!lancamentosContabeis.isEmpty()) {
			
			int count = (int) lancamentosContabeis.stream()
					.filter(lancamento -> contaContabil.equals(lancamento.getContaContabil()))
					.count();
			
			if (count > 0) {
				
				estatistica = new EstatisticaLancamentoContabil();
		
				estatistica.setQtde(count);
				
				estatistica.setMax(lancamentosContabeis.stream()
						.filter(lancamento -> contaContabil.equals(lancamento.getContaContabil()))
						.max(Comparator.comparing(LancamentoContabil::getValor)).get().getValor());
				
				estatistica.setMin(lancamentosContabeis.stream()
						.filter(lancamento -> contaContabil.equals(lancamento.getContaContabil()))
						.min(Comparator.comparing(LancamentoContabil::getValor)).get().getValor());
				
				BigDecimal total = lancamentosContabeis.stream()
						.filter(lancamento -> contaContabil.equals(lancamento.getContaContabil()))
						.map(lancamento -> lancamento.getValor())
						.reduce(BigDecimal.ZERO, BigDecimal::add);
				
				estatistica.setSoma(total);
				
				estatistica.setMedia(total.divide(new BigDecimal(estatistica.getQtde())));
			}
		}
		
		return estatistica;
	}

	public static boolean verificarExistenciaMesmoLancamento(LancamentoContabil lancamentoContabil) {
		
		return lancamentosContabeis.stream()
				.filter(lancamento -> 
					lancamentoContabil.getContaContabil().equals(lancamento.getContaContabil()) && 
					lancamentoContabil.getData().equals(lancamento.getData()) && 
					lancamentoContabil.getValor().compareTo(lancamento.getValor()) == 0)
				.count() > 0;
	}
}
