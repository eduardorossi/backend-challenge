package br.com.comexport.controller;

import java.util.List;
import java.util.UUID;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.comexport.domain.EstatisticaLancamentoContabil;
import br.com.comexport.domain.LancamentoContabil;
import br.com.comexport.service.LancamentoContabilService;

@RestController
@CrossOrigin(origins="*")
public class LancamentoContabilController {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private LancamentoContabilService service;

	@SuppressWarnings("unchecked")
	@PostMapping("/lancamentos-contabeis")
	public synchronized ResponseEntity<JSONObject> criarLancamentoContabil(@RequestBody LancamentoContabil lancamentoContabil) {
	
		try {
			
			this.logger.info("Iniciando cadastro de lancamento contabil");
			
			if (!validarLancamentoContabil(lancamentoContabil)) {
				
				if (verificarExistenciaMesmoLancamento(lancamentoContabil)) {
			
					this.logger.info("Este lancamento contabil ja existe!");
					
					return new ResponseEntity<>(HttpStatus.CONFLICT);
				}
				
				UUID id = this.service.criarLancamentoContabil(lancamentoContabil);
				
				this.logger.info("Fim do cadastro de lancamento contabil.");
				
				JSONObject json = new JSONObject();
				
				json.put("id", id);
				
				return new ResponseEntity<>(json, HttpStatus.CREATED);
			}
			
			this.logger.info("Existe(m) campo(s) nao preenchidos: Conta[" + lancamentoContabil.getContaContabil() + "], Data[" + lancamentoContabil.getData() + "], Valor[" + lancamentoContabil.getValor() + "]");
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			
		} catch (Exception e) {
			
			this.logger.info("Erro ao criar lancamento contabil. Motivo: " + e.getCause());
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	private boolean verificarExistenciaMesmoLancamento(LancamentoContabil lancamentoContabil) {
		return service.verificarExistenciaMesmoLancamento(lancamentoContabil);
	}

	private boolean validarLancamentoContabil(LancamentoContabil lancamentoContabil) {
		return StringUtils.isEmpty(lancamentoContabil.getContaContabil()) || StringUtils.isEmpty(lancamentoContabil.getData()) || StringUtils.isEmpty(lancamentoContabil.getValor());
	}

	@GetMapping("/lancamentos-contabeis/{id}")
	public ResponseEntity<LancamentoContabil> getLancamentoContabilPeloId(@PathVariable("id") UUID id) {
		
		try {
			
			this.logger.info("Iniciando busca de lancamento contabil pelo id: " + id);
			
			LancamentoContabil lancamentoContabil = this.service.getLancamentoContabilPeloId(id);
			
			HttpStatus httpStatus = HttpStatus.OK;
			
			ResponseEntity<LancamentoContabil> response = new ResponseEntity<>(lancamentoContabil, httpStatus);
					
			if (lancamentoContabil == null) {
				
				httpStatus = HttpStatus.NO_CONTENT;
				
				response = new ResponseEntity<>(httpStatus);
			}
			
			this.logger.info("Fim da busca de lancamento contabil pelo id: " + id);
			
			return response;
			
		} catch (Exception e) {
			
			this.logger.info("Erro ao buscar lancamento contabil pelo id: " + id + ". Motivo: " + e.getCause());
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping("/lancamentos-contabeis")
	public ResponseEntity<List<LancamentoContabil>> getLancamentoContabilPelaContaContabil(@RequestParam("contaContabil") String contaContabil) {
		
		try {
			
			this.logger.info("Iniciando busca de lancamento contabil pelo numero da conta: " + contaContabil);
			
			List<LancamentoContabil> lancamentosContabeis = this.service.getLancamentoContabilPelaContaContabil(contaContabil);
			
			HttpStatus httpStatus = HttpStatus.OK;
			
			ResponseEntity<List<LancamentoContabil>> response = new ResponseEntity<>(lancamentosContabeis, httpStatus);
					
			if (lancamentosContabeis.isEmpty()) {
				
				httpStatus = HttpStatus.NO_CONTENT;
				
				response = new ResponseEntity<>(httpStatus);
			}
			
			this.logger.info("Fim da busca de lancamento contabil pelo numero da conta: " + contaContabil);
			
			return response;
			
		} catch (Exception e) {
			
			this.logger.info("Erro ao buscar lancamento contabil pela conta: " + contaContabil + ". Motivo: " + e.getCause());
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping("/lancamentos-contabeis/_stats")
	public ResponseEntity<EstatisticaLancamentoContabil> getEstatisticasLancamentosContabeisPelaConta(@RequestParam(value="contaContabil", required = false) String contaContabil) {
		
		try {
			
			this.logger.info("Iniciando busca de lancamento contabil pelo numero da conta: " + contaContabil);
			
			EstatisticaLancamentoContabil estatistica = contaContabil == null 
					? this.service.getEstatisticasLancamentosContabeis() 
							: this.service.getEstatisticasLancamentosContabeis(contaContabil);
					
			HttpStatus httpStatus = HttpStatus.OK;
			
			ResponseEntity<EstatisticaLancamentoContabil> response = new ResponseEntity<>(estatistica, httpStatus);
					
			if (estatistica == null) {
				
				httpStatus = HttpStatus.NO_CONTENT;
				
				response = new ResponseEntity<>(httpStatus);
			}
					
			this.logger.info("Fim da busca de lancamento contabil pelo numero da conta: " + contaContabil);
			
			return response;
			
		} catch (Exception e) {
			
			this.logger.info("Erro ao buscar lancamento contabil pela conta: " + contaContabil + ". Motivo: " + e.getCause());
			
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
}
