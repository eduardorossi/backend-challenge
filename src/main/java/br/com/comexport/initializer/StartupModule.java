package br.com.comexport.initializer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"br.com.comexport.controller", "br.com.comexport.service"})
public class StartupModule {

	public static void main(String[] args) {
		SpringApplication.run(StartupModule.class);
	}

}
