package br.com.comexport.service;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.UUID;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.comexport.initializer.StartupModule;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { StartupModule.class })
@WebAppConfiguration
public class LancamentoContabilControllerTest {

	@TestConfiguration
    static class TestContextConfiguration {
  
        @Bean
        public LancamentoContabilService service() {
            return new LancamentoContabilService();
        }
    }
 
    @Autowired
    private LancamentoContabilService service;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    private MockMvc mockMvc;
    
    private static String id;
    
    @Before
    public void criarDadosMock() {
    	
    	mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    
    @Test
    public void criarLancamentoContabil() throws Exception {
    	
    	for (int i = 1; i < 10; i++) {
    		
    		String contaContabil = "12345";
    				
    		if (i % 2 == 0) {
    			contaContabil = "54321";
    		}
    		
    		BigDecimal valor = new BigDecimal("25.43").add(new BigDecimal(i));
    		
	    	String lancamentoContabilJson = "{\"contaContabil\":\"" + contaContabil	+ "\",\"data\":\"20180327\",\"valor\":" + valor + "}";
	    	
	    	MvcResult result = mockMvc.perform(
	    			post("/lancamentos-contabeis").
	    				content(lancamentoContabilJson).
	    				contentType(getMediaType())
	    			).
	    			andDo(print()).
	    			andExpect(status().isCreated()).andReturn();
	    	
	    	JSONObject json = new JSONObject(result.getResponse().getContentAsString());
	    	
	    	id = json.getString("id");
    	}
    }
    
    @Test
    public void testarFalhaValidacaoCamposCriacaoLancamentoContabil() throws Exception {
    	
    	String lancamentoContabilJson = "{\"contaContabil\":\"12345\",\"data\":\"\",\"valor\":25.40}";
    	
    	mockMvc.perform(
    			post("/lancamentos-contabeis").
    			content(lancamentoContabilJson).
    			contentType(getMediaType())
    			).
    	andDo(print()).
    	andExpect(status().isBadRequest());
    }
    
    @Test
    public void getLancamentoContabilPeloId() throws Exception {
    	
    	mockMvc.perform(
    			get("/lancamentos-contabeis/" + id).
    			contentType(getMediaType())
    			).
    	andDo(print()).
    	andExpect(status().isOk());
    }
    
    @Test
    public void getLancamentoContabilPeloIdHttp204() throws Exception {
    	
    	mockMvc.perform(
    			get("/lancamentos-contabeis/" + UUID.randomUUID()).
    			contentType(getMediaType())
    			).
    	andDo(print()).
    	andExpect(status().isNoContent());
    }
    
    @Test
    public void getLancamentoContabilPelaContaContabil() throws Exception {
    	
    	MvcResult result = mockMvc.perform(
    			get("/lancamentos-contabeis/").param("contaContabil", "12345").
    			contentType(getMediaType())
    			).
    	andDo(print()).
    	andExpect(status().isOk()).andReturn();
    	
    	String esperado = "["
    			+ "{\"contaContabil\":\"12345\",\"data\":\"20180327\",\"valor\":26.43}, "
    			+ "{\"contaContabil\":\"12345\",\"data\":\"20180327\",\"valor\":28.43}, "
    			+ "{\"contaContabil\":\"12345\",\"data\":\"20180327\",\"valor\":30.43}, "
    			+ "{\"contaContabil\":\"12345\",\"data\":\"20180327\",\"valor\":32.43}, "
    			+ "{\"contaContabil\":\"12345\",\"data\":\"20180327\",\"valor\":34.43}  "
    			+ "]";
    	
    	JSONAssert.assertEquals(esperado, result.getResponse().getContentAsString(), false);
    }
    
    @Test
    public void getLancamentoContabilPelaContaContabilHttp204() throws Exception {
    	
    	mockMvc.perform(
    			get("/lancamentos-contabeis/").param("contaContabil", "99999").
    			contentType(getMediaType())
    			).
    			andDo(print()).
    			andExpect(status().isNoContent());
    }
    
    @Test
    public void getEstatisticasLancamentosContabeisPelaConta() throws Exception {
    	
    	MvcResult result = mockMvc.perform(
    			get("/lancamentos-contabeis/_stats").
    			contentType(getMediaType())
    			).
    	andDo(print()).
    	andExpect(status().isOk()).andReturn();
    	
    	String esperado = "{\"soma\":273.87,\"min\":26.43,\"max\":34.43,\"media\":30.43,\"qtde\":9}";
    	
    	JSONAssert.assertEquals(esperado, result.getResponse().getContentAsString(), false);
    }
    
    @Test
    public void getEstatisticasLancamentosContabeisPelaContaComParametro() throws Exception {
    	
    	MvcResult result = mockMvc.perform(
    			get("/lancamentos-contabeis/_stats/?contaContabil=12345").
    			contentType(getMediaType())
    			).
    			andDo(print()).
    			andExpect(status().isOk()).andReturn();
    	
    	String esperado = "{\"soma\":152.15,\"min\":26.43,\"max\":34.43,\"media\":30.43,\"qtde\":5}";
    	
    	JSONAssert.assertEquals(esperado, result.getResponse().getContentAsString(), false);
    }
    
    @Test
    public void getEstatisticasLancamentosContabeisPelaContaComParametroComValorInexistente() throws Exception {
    	
    	mockMvc.perform(
    			get("/lancamentos-contabeis/_stats/?contaContabil=99999").
    			contentType(getMediaType())
    			).
    			andDo(print()).
    			andExpect(status().isNoContent());
    }
    
    @Test
    public void verificarExistenciaMesmoLancamento() throws Exception {
    	
		String contaContabil = "12345";
				
		BigDecimal valor = new BigDecimal("26.43");
		
    	String lancamentoContabilJson = "{\"contaContabil\":\"" + contaContabil	+ "\",\"data\":\"20180327\",\"valor\":" + valor + "}";
    	
    	mockMvc.perform(
    			post("/lancamentos-contabeis").
    				content(lancamentoContabilJson).
    				contentType(getMediaType())
    			).
    			andDo(print()).
    			andExpect(status().isConflict());
    }

    private MediaType getMediaType() {
    	return new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    }
}
